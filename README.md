# Quick start
## Notice this has been run on MacOS

1. Make sure you have ruby, rake, ansible installed
2. $ pip install apache-libcloud
3. going to the "APIs and Auth" section and creating a new client ID for service account. Download the generated private key in pkcs12 format and convert using the below command
  * openssl pkcs12 -in pkey.pkcs12 -passin pass:notasecret -nodes -nocerts | openssl rsa -out ~/.ssh/gce.pem
4. and change below variables in gce.yml
  * pid: bionic-spot-86820
  * email: 125023972017-trunimgkc5prcfe4v2ojk300c5r6a3hk@developer.gserviceaccount.com
  * pem: /Users/cmn6765/.ssh/gce-cmn6765.pem

### sample run
```
╰─➤  rake

PLAY [Google Compute Engine Nginx] ********************************************

TASK: [Create GCE VM] *********************************************************
changed: [localhost]

TASK: [Wait for ssh service] **************************************************
ok: [localhost] => (item={u'status': u'RUNNING', u'network': u'default', u'zone': u'us-central1-a', u'tags': [u'nginx', u'nginx-pool'], u'image': None, u'disks': [u'nginx1'], u'public_ip': u'130.211.113.111', u'private_ip': u'10.240.230.57', u'machine_type': u'f1-micro', u'metadata': {}, u'name': u'nginx1'})
ok: [localhost] => (item={u'status': u'RUNNING', u'network': u'default', u'zone': u'us-central1-a', u'tags': [u'nginx', u'nginx-pool'], u'image': None, u'disks': [u'nginx2'], u'public_ip': u'104.154.82.93', u'private_ip': u'10.240.227.19', u'machine_type': u'f1-micro', u'metadata': {}, u'name': u'nginx2'})

TASK: [Add host to groupname] *************************************************
ok: [localhost] => (item={u'status': u'RUNNING', u'network': u'default', u'zone': u'us-central1-a', u'tags': [u'nginx', u'nginx-pool'], u'image': None, u'disks': [u'nginx1'], u'public_ip': u'130.211.113.111', u'private_ip': u'10.240.230.57', u'machine_type': u'f1-micro', u'metadata': {}, u'name': u'nginx1'})
ok: [localhost] => (item={u'status': u'RUNNING', u'network': u'default', u'zone': u'us-central1-a', u'tags': [u'nginx', u'nginx-pool'], u'image': None, u'disks': [u'nginx2'], u'public_ip': u'104.154.82.93', u'private_ip': u'10.240.227.19', u'machine_type': u'f1-micro', u'metadata': {}, u'name': u'nginx2'})

PLAY [provisioning GCE VM] ****************************************************

GATHERING FACTS ***************************************************************
ok: [104.154.82.93]
ok: [130.211.113.111]

TASK: [nginx | install nginx] *************************************************
changed: [130.211.113.111]
changed: [104.154.82.93]

TASK: [nginx | index.html template] *******************************************
changed: [130.211.113.111]
changed: [104.154.82.93]

PLAY [forwarding rule] ********************************************************

TASK: [forwarding port 80] ****************************************************
ok: [localhost]

TASK: [Load Balance] **********************************************************
changed: [localhost]

TASK: [command echo item] *****************************************************
changed: [localhost -> 127.0.0.1] => (item=104.197.18.41)

PLAY RECAP ********************************************************************
104.154.82.93              : ok=3    changed=2    unreachable=0    failed=0
130.211.113.111            : ok=3    changed=2    unreachable=0    failed=0
localhost                  : ok=6    changed=3    unreachable=0    failed=0

╭─cmn6765@cmn6765.local ~/git-repo/googlecloud
╰─➤  curl 104.197.18.41
nginx1 - 10.240.77.168
```
